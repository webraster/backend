FROM alpine:edge
MAINTAINER voidreturn

RUN apk add --no-cache nodejs-npm

COPY worker.js /root/worker.js

CMD ["node", "/root/worker.js"]
