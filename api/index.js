const chrome = require('chrome-aws-lambda')
const puppeteer = require('puppeteer-core')
const express = require('express')
const app = express()
app.use(express.json())

app.get('/screenshot', async (req, res) => {
  const DEFAULT_WIDTH = 900
  const DEFAULT_HEIGHT = 900
  const browser = await puppeteer.launch({
    args: chrome.args,
    executablePath: await chrome.executablePath,
    headless: chrome.headless,
  })
  const page = await browser.newPage()
  await page.setViewport({
    width: DEFAULT_WIDTH,
    height: DEFAULT_HEIGHT,
    deviceScaleFactor: 1,
  })
  await page.goto(req.query.url, { waitUntil : 'networkidle0' })
  await new Promise(r => setTimeout(r, 500))
  const screenshotData = await page.screenshot({
    clip: { x: 0, y: 0, width: DEFAULT_WIDTH, height: DEFAULT_HEIGHT },
  })
  res.set('Cache-Control', 's-maxage=86400, max-age=3600')
  if (req.query.width && req.query.height) {
    const sharp = require('sharp')
    const data = await sharp(screenshotData)
      .resize(+req.query.width, +req.query.height)
      .png()
      .toBuffer()
    return res.end(data, 'binary')
  }
  res.end(screenshotData, 'binary')
})

module.exports = app
